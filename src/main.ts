import App from "./App.vue";
import Vue from "vue";
import router from "./router";
import store from "./store";
import vuetify from "@/plugins/vuetify";
import { firestorePlugin } from "vuefire";

Vue.use(firestorePlugin);

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount("#app");
