import firebase from "firebase/app";

import { alivenessPeriodInSeconds } from "@/constants";
import { legalVote, VoteModel, Votes } from "@/model/model";

function getTimeDiff(time: firebase.firestore.Timestamp): number {
  return firebase.firestore.Timestamp.now().seconds - time.seconds;
}

export function isOldVote(vote: VoteModel): boolean {
  return getTimeDiff(vote.timestamp) > alivenessPeriodInSeconds + 1;
}

export function filterOldVotes(votes: Votes): Votes {
  return votes.filter(vote => !isOldVote(vote));
}

export function nRequiredVotes(votes: Votes): number {
  return filterOldVotes(votes).length;
}

function filterLegalVotes(votes: Votes): Votes {
  return votes.filter(vote => vote.vote !== undefined);
}

export function nCastVotes(votes: Votes): number {
  return filterLegalVotes(filterOldVotes(votes)).length;
}

export function countVotes(votes: Votes): number[] {
  const countedVotes: { [key in legalVote]: number } = {
    "0": 0,
    "0.5": 0,
    "1": 0,
    "1.5": 0,
    "2": 0,
    "2.5": 0,
    "3": 0,
    "5": 0,
    "?": 0
  };

  for (const vote of votes) {
    if (vote.vote === undefined) {
      continue;
    }
    countedVotes[vote.vote] += 1;
  }

  const sortedKeys: legalVote[] = Object.keys(
    countedVotes
  ).sort() as legalVote[]; // type information is lost after Object.keys()
  return sortedKeys.map(key => countedVotes[key]);
}

export function isVoteByUser(votes: Votes, userId: string): string | undefined {
  for (const vote of votes) {
    if (vote.userId === userId) {
      return vote.vote;
    }
  }
}
