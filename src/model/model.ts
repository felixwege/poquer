import firebase from "firebase/app";
require("firebase/auth");

export const legalVotes = [
  "0",
  "0.5",
  "1",
  "1.5",
  "2",
  "2.5",
  "3",
  "5",
  "?"
] as const;
export type legalVote = typeof legalVotes[number];

export interface VoteModel {
  userId: string;
  timestamp: firebase.firestore.Timestamp;
  vote?: legalVote;
}

export type Votes = VoteModel[];

export interface VoteModelDB {
  [userId: string]: {
    timestamp: firebase.firestore.Timestamp;
    vote?: legalVote;
  };
}

export function voteModelToDB(voteData: VoteModel): VoteModelDB {
  if (voteData.vote === undefined) {
    return {
      [voteData.userId]: { timestamp: voteData.timestamp }
    };
  }

  return {
    [voteData.userId]: { timestamp: voteData.timestamp, vote: voteData.vote }
  };
}

export function votesFromDB(dbVotes: {}): Votes {
  if (!dbVotes) {
    return [];
  }

  const votes: Votes = [];
  for (const [key, value] of Object.entries(dbVotes)) {
    const voteData = value as VoteModel;
    voteData.userId = key;
    votes.push(voteData);
  }
  return votes;
}
