import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#54B46D",
        secondary: "#3C4D80",
        accent: "#F08D49",
        info: "#EEEEEE",
        background: "#FFFFFF"
      },
      dark: {
        primary: "#54B46D",
        secondary: "#3C4D80",
        accent: "#F08D49",
        info: "#888888",
        background: "#222222"
      }
    }
  }
});
