import firebase from "firebase/app";
import "firebase/firestore";
import { v4 as uuid } from "uuid";

const firebaseConfig = {
  apiKey: "AIzaSyCMbXT4UGF2Zloz7MNxJjUl4kUU2-2Rask",
  authDomain: "poquer-f0e16.firebaseapp.com",
  projectId: "poquer-f0e16",
  storageBucket: "poquer-f0e16.appspot.com",
  messagingSenderId: "667381644919",
  appId: "1:667381644919:web:42031f6bcece693de41072",
  measurementId: "G-VGDSLPM3P6"
};

export const db = firebase.initializeApp(firebaseConfig).firestore();
export const tables = db.collection("tables");

export async function signInAnonymously(): Promise<string> {
  const response = await firebase.auth().signInAnonymously();
  return response.user?.uid ?? uuid();
}
