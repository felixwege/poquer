import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import firebase from "firebase/app";
import { firestoreAction, vuexfireMutations } from "vuexfire";

import { VoteModel, voteModelToDB, Votes, votesFromDB } from "@/model/model";
import { signInAnonymously, tables } from "@/db";
import { filterOldVotes } from "@/business/votes";

Vue.use(Vuex);

interface StoreModel {
  userId: string;
  isUserAuthenticated: boolean;
  tableId: string;
  _votes: {};
}

export default new Vuex.Store<StoreModel>({
  plugins: [createPersistedState()],
  state: {
    userId: "",
    isUserAuthenticated: false,
    tableId: "",
    _votes: {} // "private"; use getter instead
  },
  getters: {
    votes(state): Votes {
      return filterOldVotes(votesFromDB(state._votes));
    }
  },
  mutations: {
    setTableId(state, id) {
      state.tableId = id;
    },
    setUserId(state, id) {
      state.userId = id;
      state.isUserAuthenticated = true;
    },
    async resetVote(state, vote: VoteModel) {
      const voteData: VoteModel = {
        userId: vote.userId,
        timestamp: firebase.firestore.Timestamp.now(),
        vote: undefined
      };
      return tables.doc(state.tableId).update(voteModelToDB(voteData));
    },
    ...vuexfireMutations
  },
  actions: {
    bindTable: firestoreAction(({ bindFirestoreRef, state }) => {
      return bindFirestoreRef("_votes", tables.doc(state.tableId));
    }),
    async createTable({ state, commit }) {
      if (!state.isUserAuthenticated) {
        commit("setUserId", await signInAnonymously());
      }

      const voteData: VoteModel = {
        userId: state.userId,
        timestamp: firebase.firestore.Timestamp.now()
      };
      const ref = await tables.add(voteModelToDB(voteData));
      commit("setTableId", ref.id);
    },
    async joinTable({ state, commit, dispatch }, id) {
      if (!state.isUserAuthenticated) {
        commit("setUserId", await signInAnonymously());
      }

      const voteData: VoteModel = {
        userId: state.userId,
        timestamp: firebase.firestore.Timestamp.now()
      };
      const exists = (await tables.doc(id).get()).exists;
      if (exists) {
        await tables.doc(id).update(voteModelToDB(voteData));
      } else {
        await tables.doc(id).set(voteModelToDB(voteData));
      }
      commit("setTableId", id);
      dispatch("bindTable");
    },
    async sendAliveness({ state }) {
      await tables.doc(state.tableId).update({
        [`${state.userId}.timestamp`]: firebase.firestore.Timestamp.now()
      });
    },
    async vote({ state, commit }, value) {
      if (!state.isUserAuthenticated) {
        commit("setUserId", await signInAnonymously());
      }

      const voteData: VoteModel = {
        userId: state.userId,
        timestamp: firebase.firestore.Timestamp.now(),
        vote: value
      };
      await tables.doc(state.tableId).update(voteModelToDB(voteData));
    },
    async resetTable({ getters, commit }) {
      await Promise.all(
        getters.votes.map((vote: VoteModel) => {
          return commit("resetVote", vote);
        })
      );
    }
  },
  modules: {}
});
